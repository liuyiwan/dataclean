# 连享会直播课：<br>Stata 数据清洗之实战操作（二）

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/海报：数据清洗V5.png)

&emsp;

---

**目录**
[[TOC]]

---

&emsp;

## 1. 课程导言

数据清洗是实证研究的一个重要组成部分。为了提高数据处理效率，连享会于 2020 年 7 月分享了直播课：[Stata 数据清洗之实战操作 (一)](https://www.lianxh.cn/news/f785de82434c1.html)，以 **World Development Indicators (WDI)** 数据库为例，对 Stata 中的文件操作、循环语句、横向合并与纵向追加等功能进行了讲解。

课后与部分学员的交流中发现，对于数据处理的一些细节还存在较多疑惑。例如，多源数据合并时关键变量的统一、日期型数据格式转换、异常值识别与处理、缺失数据插补等。

为此，我们推出了第二季课程：**Stata 数据清洗之实战操作 (二)**。

本课程是此前课程的进一步细化，从更微观细化角度对数据清洗中的一些常见问题进行讲解。涉及多源数据合并、文字变量格式统一、异常值与缺失值处理、日期型格式数据统一等，主要内容包括：
- **1. 异常值识别与处理**。何为异常值，异常值包括哪些情形，如何处理。计算 COOK'D， DFBETAS 和 DFFITS 进行异常值识别。
- **2. 重复值识别与处理**。有些操作要求一些变量值的唯一性，比如数据合并操作中，需保证关键变量的一致性。该部分将对重复值查询、重复值删除等操作进行讲解。
- **3. 不一致数据处理**。不一致特征多出现于文字变量 (例如：北京与北京市、有限公司与有限责任公司)。本部分主要讲解字符操作函数，包括删除、截取、替代、提取等。
- **4. 缺失数据处理**。包括缺失值成因、缺失值类型、缺失值识别与缺失值处理等。
- **5. 日期型数据处理**。讲解原始日期变量为字符型和数值型变量时，如何将不同格式数据转换为 Stata 可识别的格式，不同频率数据之间的相互转换，及日期间隔计算等。

本课程遵循 **「以小见大」** 原则，通过实例讲解，力求能够覆盖绝大部分的数据处理方法。

---

&emsp;


> ### 第二季：有哪些新东西？

&emsp;

> [**第一季：**](https://www.lianxh.cn/news/f785de82434c1.html) 知其然
- 引导大家建立数据清晰地分析思路和流程
- 概括性介绍了数据清洗的基本步骤：数据读取、数据类型变换、缺失值处理、离群值处理、取对数操作等
- 重点讲解了数据清洗的准备工作：数据读取、文件和文件夹程序化管理、横向合并与纵向追加等问题

> [**第二季**：](https://lianxh.duanshu.com/#/brief/course/23924488072b4e458ec3bb0a830b187f) 知其所以然
- 掌握各种操作背后的原理，做研究「设计者」而不是「操作者」
- 以离群值为例：会区分被解释变量和解释变量包含离群值的处理方法有何差异、各类处理方法的异同及其对结果解释的影响；
- 以缺失值为例：会从缺失值的产生原因讲起，这样才能确认缺失值是否会导致样本选择偏误，进而导致内生性问题，并对应选择合适的应对方法 —— 插值、补漏还是借助特定的计量模型。

概括起来说，此次课程可以看成上次课程的进一步深入，有着 **[承上启下]** 的关系。上次课程以数据前期处理为重点，这次课程以数据处理过程中具体细节为重点，让你从审稿专家的视角应对数据清洗问题。

---

&emsp;

## 2. 课程概览

- **听课方式：** 网络直播。支持手机、iPad、电脑等。
- **观看方式：** 
  - **温馨提示：** 您需要用手机号或微信号注册短书账号
  - **手机/iPad** (直接点击链接即可观看)：<https://lianxh.duanshu.com/#/brief/course/23924488072b4e458ec3bb0a830b187f>；
  - **电脑** (将课程链接复制到电脑浏览器打开，右上角点击登陆即可)：<https://lianxh-pc.duanshu.com/course/detail/23924488072b4e458ec3bb0a830b187f>
- **直播嘉宾**：游万海 老师 (福州大学)
- **费用/软件**：88 元，Stata
- **时间**：2020 年 11 月 28 日，19:00-21:00
- **课程咨询：** 李老师-18636102467（微信同号）

---

&emsp;

## 3. 课程提要

- 不规则数据（异常值）
  - 识别方法: 描述统计法；图示法；指标法
  - 处理方法
- 不必要数据（重复值）：识别与删除
- 不一致数据
  - 字符函数（删除空格，字符截取，大小写统一，字段提取等）
  - 字符距离计算
- 日期型数据
  - 格式统一
  - 格式转换
- 缺失数据
  - 缺失值成因
  - 缺失值类型：完全随机缺失、随机缺失、非随机缺失
  - 识别缺失值
  - 缺失值处理：删除；插补（邻近非缺失值、均值/中位数、线性插值、线性拟合等）
- 应用实例

---

&emsp;

## 4. 课程特色

- 短小精悍：通过实例演示掌握常用的数据处理方法。
- 讲义程序：分享电子版课件 (数据和程序)，课程中的方法可以应用于自己的论文中。
- 课后答疑：课程结束后，授课老师将提供问题解答。

---

&emsp;

## 5. 嘉宾简介

**游万海**，管理学博士，福州大学经济与管理学院副教授，主要研究领域为空间计量模型、分位数回归模型及应用，以在 World Development, Energy Economics, Economics Letters, Journal of Cleaner Product， 统计研究 等期刊发表 30 余篇论文。担任 Energy Economics, Economic Modelling, Finance Research Letters 等期刊匿名审稿人。游老师讲授的「文本分析与爬虫专题」受到了学员的一致好评。

<img style="width: 150px" src="https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/游万海工作照.jpeg">

---

&emsp;

## 6. 相关资料

- **预习资料：** [连享会视频公开课 - Stata33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0)：
- **连享会推文：** [数据处理专题]()
- **进阶内容：** [Stata 寒假班](https://gitee.com/arlionn/PX) 对暂元、循环语句、数据合并等会有进一步的讲解。

&emsp;

&emsp;

&emsp;

&emsp;

## 相关课程

&emsp;

> **连享会-直播课** 上线了！         
>  <http://lianxh.duanshu.com>  

> **免费公开课：**
> - [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) - 连玉君，时长：1小时40分钟
> - [Stata 33 讲](https://lianxh.duanshu.com/#/brief/course/b22b17ee02c24015ae759478697df2a0) - 连玉君, 每讲 15 分钟. 
> - 部分直播课 [课程资料下载](https://gitee.com/arlionn/Live) (PPT，dofiles等)  



&emsp;

---
### 课程一览   


> 支持回看，所有课程可以随时购买观看。

| 专题 | 嘉宾    | 直播/回看视频    |
| --- | --- | --- |
| &#x2B50; **[最新专题](https://www.lianxh.cn/news/46917f1076104.html)** &#x2B50;|  | DSGE, 因果推断, 空间计量等  |
| &#x2B55; **[Stata数据清洗](https://lianxh.duanshu.com/#/brief/course/c5193f0e6e414a7e889a8ff9aeb4aaef)** | 游万海| [直播, 2 小时](https://www.lianxh.cn/news/f785de82434c1.html)，已上线 |
| 研究设计 | 连玉君    | [我的特斯拉-实证研究设计](https://lianxh.duanshu.com/#/course/5ae82756cc1b478c872a63cbca4f0a5e)，[-幻灯片-](https://gitee.com/arlionn/Live/tree/master/%E6%88%91%E7%9A%84%E7%89%B9%E6%96%AF%E6%8B%89-%E5%AE%9E%E8%AF%81%E7%A0%94%E7%A9%B6%E8%AE%BE%E8%AE%A1-%E8%BF%9E%E7%8E%89%E5%90%9B)|
| 面板模型 | 连玉君    | [动态面板模型](https://efves.duanshu.com/#/brief/course/3c3ac06108594577a6e3112323d93f3e)，[-幻灯片-](https://quqi.gblhgk.com/s/880197/o7tDK5tHd0YOlYJl)   |
| 面板模型 | 连玉君  | [直击面板数据模型](https://lianxh.duanshu.com/#/brief/course/7d1d3266e07d424dbeb3926170835b38) [免费公开课，2小时]  |


> Note: 部分课程的资料，PPT 等可以前往 [连享会-直播课](https://gitee.com/arlionn/Live) 主页查看，下载。

&emsp;

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom01.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom02.png)

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/lianxhbottom03.png)

&emsp;

---
>#### 关于我们

- **Stata连享会** 由中山大学连玉君老师团队创办，定期分享实证分析经验。[直播间](http://lianxh.duanshu.com) 有很多视频课程，可以随时观看。
- [连享会-主页](https://www.lianxh.cn) 和 [知乎专栏](https://www.zhihu.com/people/arlionn/)，300+ 推文，实证分析不再抓狂。
- **公众号推文分类：** [计量专题](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=4&sn=0c34b12da7762c5cabc5527fa5a1ff7b) | [分类推文](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=2&sn=07017b31da626e2beab0332f5aa5f9e2) | [资源工具](https://mp.weixin.qq.com/mp/homepage?__biz=MzAwMzk4ODUzOQ==&hid=3&sn=10c2cf37e172289644f03a4c3b5bd506)。推文分成  **内生性** | **空间计量** | **时序面板** | **结果输出** | **交乘调节** 五类，主流方法介绍一目了然：DID, RDD, IV, GMM, FE, Probit 等。
- **公众号关键词搜索/回复** 功能已经上线。大家可以在公众号左下角点击键盘图标，输入简要关键词，以便快速呈现历史推文，获取工具软件和数据下载。常见关键词：`课程, 直播, 视频, 客服, 模型设定, 研究设计, stata, plus, 绘图, 编程, 面板, 论文重现, 可视化, RDD, DID, PSM, 合成控制法` 等

---

![连享会主页  lianxh.cn](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会跑起来就有风400.png "连享会主页：lianxh.cn")

&emsp; 

> 连享会小程序：扫一扫，看推文，看视频……

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会小程序二维码180.png)

&emsp; 

> 扫码加入连享会微信群，提问交流更方便

![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/连享会-学习交流微信群001-150.jpg)

> &#x270F;  连享会学习群-常见问题解答汇总：  
> &#x2728;  <https://gitee.com/arlionn/WD>  




